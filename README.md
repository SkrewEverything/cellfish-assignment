# CellFish Assignment

## Assumptions:
- User has only 1 number(To make things simpler)
- Data of the user from the database can be retieved using an **id**(As the assumption is that the user has only 1 phone number, we can use it as primary key or for scalability email is suitable for primary key as user can have multiple numbers)
- The plans are retrieved based on phone number's `location`(circle) and `operator`(company)
- For the assignment, the data is retrieved from JSON present on the disk `User.json` and `Plan.json`. For parsing json, I have used `JSONSimple` library.

## Interface
 - `Plan`: It stores the info of the plan. This `interface` should be implemented for storing the plan info 
 - `PlanRequest`: This is the basic interface for getting plan data from the source. 
 - `User`: It stores the info of the user. This `interface` should be implemented for storing the user info
 - `UserRequest`: This is the basic interface for getting user info. This `interface` should be implemented for retrieving the user info 
 - `BestPlan`: Calculates the best plan suitable for the user. This `interface` should be implemented for calculating the Best Plan for the user

## Classes:
- `PlanInfo`: Implements `Plan`
- `PlanRequestOffline`:  Implements `PlanRequest`. Gets data from disk
- `UserInfo`: Implements `User`
- `UserRequestOffline`: Implements `UserRequest`. Gets data from disk
- `CalculateBestPlan`:  Implements `BestPlan`
- `UserFactory`: UserFactory deals with multiple sources(for retrieving data) and different types of data formats. It parses the data from source and returns the `User` object
- `PlanFactory`: PlanFactory deals with multiple sources(for retrieving data) and different types of data formats. It parses the data from source and returns the `Plan` object


## Algorithm:
The basic idea is to compare 3 variants of the packs
1. A pack in which we consider both local and std rates to calculate `costPerDayMixed` based on the user usage.
2. A pack in which we only consider local rates(Here we consider usage of std mins is 0) to calculate `costPerDayLocal` based only on the users local usage.
3. A pack in which we only consider std rates(Here we consider usage of local mins is 0) to calculate `costPerDaySTD` based only on the users std usage.

Now we take the minimum of `costPerDayMixed`, `costPerDayLocal` and `costPerDaySTD` from the available plans.
```
If the users std usage is 0
    then only consider costPerDayMixed and costPerDayLocal
    return which ever costs less. If both the packs cost same, then return costPerDayMixed(Because if the mixed pack provides std also(it may or may not) the user gets benifitted. If there is no std offer in the mixed pack, then we will anyway get costPerDayLocal as the std mins is 0 while calculating costPerDayMixed)
    
    
If the users local usage is 0
    then follow the same rules as above. Instead of costPerDayLocal consider constPerDaySTD
    
    
If the user has local and std usage
    then compare (costPerDayMixed <= costPerDayLocal + costPerDaySTD) and return the smaller one
    
```