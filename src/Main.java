import org.json.simple.*;
import org.json.simple.parser.*;

import java.io.*;
import java.util.*;

import com.company.plan.*;
import com.company.user.*;
import com.company.calculate.*;

public class Main
{

	public static void main(String[] args) throws Exception, IOException, ParseException
	{

		// Instantiate user object
        User user = UserFactory.getUserFromJSON("john@mail.com");
        //System.out.println(user);
 
        // Instantiate plans object for the above user
        List<Plan> plans = PlanFactory.getPlansFromJSON(user.getLocation(),	user.getOperator());
        //System.out.println(plans);
        
        // Calculate the best plan for the user from the available plans
        BestPlan bp = new CalculateBestPlan(user, plans);
        System.out.println(bp.getBestPlan());
        

	}

}
