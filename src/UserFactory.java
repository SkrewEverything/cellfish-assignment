import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.company.plan.PlanInfo;
import com.company.plan.Plan;
import com.company.user.UserInfo;
import com.company.user.UserRequestOffline;


/**
 * UserFactory deals with multiple sources(for retrieving data) and different types of data formats.
 * 
 * It parses the data from source and returns the `User` object
 * @author sriharish
 *
 */
public class UserFactory
{
	/**
	 * Retrieves the data from the disk, parses JSON and returns `User` object
	 * @param id To uniquely identify/query a user
	 * @return `User` object
	 */
	public static UserInfo getUserFromJSON(String id)
	{
		JSONObject json = new UserRequestOffline(id).getUserInfo();
		List<Plan> plans = new ArrayList<>();
		
		String operator = (String) json.get("operator");
		String location = (String) json.get("location");
		String name = (String) json.get("name");
		int phoneNumber = Integer.parseInt((String) json.get("phone number"));
		int[] usage = new int[2];
    	usage[0] = Integer.parseInt((String) json.get("local usage"));
    	usage[1] = Integer.parseInt((String) json.get("std usage"));
		
        JSONArray a = (JSONArray)json.get("plans");
        for(int i = 0; i<a.size(); i++)
        {
        	JSONObject temp = (JSONObject)a.get(i);
        	int cost = Integer.parseInt((String) temp.get("cost"));
        	if(cost == -1)
        	{
        		continue;
        	}
        	else
        	{
        		int[] callRates = new int[2];
            	callRates[0] = Integer.parseInt((String) temp.get("local rate"));
            	callRates[1] = Integer.parseInt((String) temp.get("std rate"));
            	int validity = Integer.parseInt((String) temp.get("validity"));
            	
            	plans.add(new PlanInfo(operator, location, cost, callRates, validity));
        	}
        	
        }
        
		return new UserInfo(name, id, usage, location, operator, phoneNumber, plans);
	}
}
