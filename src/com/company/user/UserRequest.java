package com.company.user;

/**
 * This is the basic interface for getting user info.
 * 
 * This `interface` should be implemented for retrieving the user info 
 * 
 * The implementation will be different obviously.
 * Like getting data from database using JDBC
 * Getting data from HTTP server
 * Getting data from disk
 * 
 * @author sriharish
 *
 */
public interface UserRequest
{
	/**
	 * Gets the data from the source
	 * @return An Object which contains the data of the user
	 */
	Object getUserInfo();
	
	/**
	 * Returns the id of the current user
	 * @return id of the current user
	 */
	String getCurrentUserID();
	
	/**
	 * Sets/Updates the id of the current user
	 * @param id id for the current user
	 */
	void setCurrentUserID(String id);
}
