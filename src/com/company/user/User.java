package com.company.user;

import java.util.List;

import com.company.plan.Plan;

/**
 * It stores the info of the user
 * 
 * This `interface` should be implemented for storing the user info 
 * @author sriharish
 *
 */
public interface User
{
	/**
	 * This gives all the plans the user is currently subscribed to
	 * @return It gives the `List` of `Plan`s that the user is currently subscribed to
	 */
	List<Plan> getPlanDetails();
	
	/**
	 * Returns the phone number of the user 
	 * @return The phone number of the user
	 */
	int getPhoneNumber();
	
	/**
	 * [0] -> Gives the usage of Local mins
	 * [1] -> Gives the usage of STD mins
	 * @return The mobile usage of the user in mins
	 */
	int[] getUsage();
	
	/**
	 * Returns the name of the user
	 * @return The name of the user
	 */
	String getName();
	
	/**
	 * Returns the email of the user
	 * @return The email of the user
	 */
	String getEmail();
	
	/**
	 * Returns the location of the user's phone number
	 * @return The location/circle of the phone number
	 */
	String getLocation();
	
	/**
	 * Returns the operator of the user's phone number
	 * @return The operator/company of the phone number
	 */
	String getOperator();
	
}
