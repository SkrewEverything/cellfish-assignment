package com.company.user;


import java.util.List;

import com.company.plan.Plan;

/**
 * It stores the info of the user
 * @author sriharish
 *
 */
public class UserInfo implements User
{
	private String name;
	private String email;
	private int[] usage = new int[2];
	private String location;
	private String operator;
	private int phoneNumber;
	private List<Plan> planDetails;
	
	public UserInfo(String name, String email, int[] usage, String location, String operator, int phoneNumber, List<Plan> planDetails)
	{
		this.name = name;
		this.email = email;
		this.usage = usage;
		this.location = location;
		this.operator = operator;
		this.phoneNumber = phoneNumber;
		this.planDetails = planDetails;
	}

	@Override
	public List<Plan> getPlanDetails()
	{
		return this.planDetails;
	}

	@Override
	public int getPhoneNumber()
	{
		return this.phoneNumber;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public String getEmail()
	{
		return this.email;
	}
	
	@Override
	public int[] getUsage()
	{
		return this.usage;
	}

	@Override
	public String getLocation()
	{
		return this.location;
	}

	@Override
	public String getOperator()
	{
		return this.operator;
	}
	
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Name: " + this.name + ", ");
		sb.append("Email: " + this.email + ", ");
		sb.append("Phone Number: " + this.phoneNumber + ", ");
		sb.append("Operator: " + this.operator + ", ");
		sb.append("Location: " + this.location + ", ");
		sb.append("Local Usage: " + this.usage[0] +" mins " + "STD Usage: " + this.usage[1] + " mins\n");
		sb.append("===Plan Details===\n");
		sb.append(this.planDetails.toString());
		return sb.toString();
	}


}
