package com.company.user;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * It gets the data from the disk
 * @author sriharish
 *
 */
public class UserRequestOffline implements UserRequest
{
	public String id;
	
	private UserRequestOffline()
	{
		// To force the user to inistantiate this class by providing user id
	}
	
	public UserRequestOffline(String id)
	{
		this.id = id;
	}
	

	@Override
	public JSONObject getUserInfo()
	{
		Object obj = null;
		try
		{
			obj = new JSONParser().parse(new FileReader("./data/User.json"));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		} 
        
        // typecasting obj to JSONObject 
        return (JSONObject) obj; 
	}

	@Override
	public String getCurrentUserID()
	{
		return this.id;
	}

	@Override
	public void setCurrentUserID(String id)
	{
		this.id = id;
		
	}

}
