package com.company.calculate;

import java.util.List;

import com.company.plan.Plan;

/**
 * Calculates the best plan suitable for the user
 * 
 * This `interface` should be implemented for calculating the Best Plan for the user 
 * @author sriharish
 *
 */
public interface BestPlan
{
	/**
	 * Calculates the best plan suitable for the user 
	 * 
	 * @return List<Plan> Returns best plans for a specific user
	 */
	List<Plan> getBestPlan();
}
