package com.company.calculate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.company.plan.Plan;
import com.company.user.User;

/**
 * Calculates the best plan based on the available plans and user monthly usage
 * @author sriharish
 *
 */
public class CalculateBestPlan implements BestPlan
{
	private List<Plan> plans;
	private User user;
	private List<Cost> costs;
	
	/**
	 * 
	 * @param user Info about the user
	 * @param plans	All the available plans for a specific user
	 */
	public CalculateBestPlan(User user, List<Plan> plans)
	{
		this.user = user;
		this.plans = plans;
		List<Cost> costs = new ArrayList<>();
		for(int i = 0; i < plans.size(); i++)
		{
			costs.add(new Cost(user, plans.get(i)));
		}
		this.costs = costs;
		
	}

	@Override
	/**
	 * Calculates the best plan suitable for the user 
	 * 
	 * @return List<Plan> Returns best plans for a specific user
	 */
	public List<Plan> getBestPlan()
	{
		/**
		 * We can sort the lists using custom comparator in O(nlogn).
		 * 
		Collections.sort(this.costs, Cost.getCompByMixedRates());
		Cost mixedCost = this.costs.get(0);
		Collections.sort(this.costs, Cost.getCompByLocalRates());
		Cost localCost = this.costs.get(0);
		Collections.sort(this.costs, Cost.getCompBySTDRates());
		Cost stdCost = this.costs.get(0);
		*/
		
		/** Or We can just get the minimum in just 1 pass in O(n) */
		Cost mixedCost = this.costs.get(0);
		Cost localCost = this.costs.get(0);
		Cost stdCost = this.costs.get(0);
		for(int i = 1; i < this.costs.size(); i++)
		{
			if(this.costs.get(i).costPerDayMixed < mixedCost.costPerDayMixed)
			{
				mixedCost = this.costs.get(i);
			}
			
			if(this.costs.get(i).costPerDayLocal < localCost.costPerDayLocal)
			{
				localCost = this.costs.get(i);
			}
			
			if(this.costs.get(i).costPerDaySTD < stdCost.costPerDaySTD)
			{
				stdCost = this.costs.get(i);
			}
		}

		List<Plan> bestPlan = new ArrayList<>();
		
		if(user.getUsage()[0] < 1) // If the local usage of the user is 0, then we don't consider the "only local pack"
		{
			if(mixedCost.costPerDayMixed <= stdCost.costPerDayLocal) // If the user is getting "mixed pack" with the same rate as "only std pack" then the user is benefitted to subscribe to "mixed pack"
			{
				bestPlan.add(mixedCost.plan);
				return bestPlan;
			}
			else
			{
				bestPlan.add(stdCost.plan);
				return bestPlan;
			}
		}
		else if(user.getUsage()[1] < 1) // If the std usage of the user is 0, then we don't consider the "only std pack"
		{
			if(mixedCost.costPerDayMixed <= localCost.costPerDayLocal) // If the user is getting "mixed pack" with the same rate as "only local pack" then the user is benefitted to subscribe to "mixed pack"
			{
				bestPlan.add(mixedCost.plan);
				return bestPlan;
			}
			else
			{
				bestPlan.add(localCost.plan);
				return bestPlan;
			}
		}
		else // If the user has both local and std usage
		{
			if(mixedCost.costPerDayMixed <= localCost.costPerDayLocal + stdCost.costPerDaySTD) // We check whether subscribing to 2 separate packs will decrese the cost for the user instead of subscribing to only 1 pack
			{
				bestPlan.add(mixedCost.plan);
				return bestPlan;
			}
			else
			{
				bestPlan.add(localCost.plan);
				bestPlan.add(stdCost.plan);
				return bestPlan;
			}
		}
		
	}
	
}

/**
 * A Custom class to calculate the costPerDay variants of the plans
 * @author sriharish
 *
 */
class Cost
{
	int cost;
	int localRate;
	int stdRate;
	int validity;
	int localUsage;
	int stdUsage;
	double costPerDayMixed;
	double costPerDayLocal;
	double costPerDaySTD;
	Plan plan;
	
	/**
	 * 
	 * @param user The user for which the cost is being calculated
	 * @param plan	The plan for which the cost is being calculated
	 */
	Cost(User user, Plan plan)
	{
		this.plan = plan;
		this.cost = plan.getCost();
		int[] rates = plan.getCallRates();
		this.localRate = rates[0];
		this.stdRate = rates[1];
		this.validity = plan.getValidity();
		int[] usage = user.getUsage();
		this.localUsage = usage[0];
		this.stdUsage = usage[1];
		this.costPerDayMixed = this.calculateCostPerDayMixed();
		this.costPerDayLocal = this.calculateCostPerDayLocal();
		this.costPerDaySTD = this.calculateCostPerDaySTD();
	}
	
	/**
	 * 
	 * @return The cost per day considering local and std
	 */
	private double calculateCostPerDayMixed()
	{
		int local = this.localRate * this.localUsage;
		int std = this.stdRate * this.stdUsage;
		double result = ( this.cost + ( (local + std)/100.0) ) / this.validity;
		
		return result;
	}
	
	/**
	 * 
	 * @return The cost per day considering only local
	 */
	private double calculateCostPerDayLocal()
	{
		int local = this.localRate * this.localUsage;
		double result = ( this.cost + ( (local)/100.0) ) / this.validity;
		
		return result;
	}
	
	/**
	 * 
	 * @return The cost per day considering only std
	 */
	private double calculateCostPerDaySTD()
	{
		int std = this.stdRate * this.stdUsage;
		double result = ( this.cost + ( (std)/100.0) ) / this.validity;
		
		return result;
	}
	
	/**
	 * This is comparator is used to sort the List<Cost> in ascending order according to the mixed rates
	 * @return comparator to use in Collections.sort()
	 */
	public static Comparator<Cost> getCompByMixedRates() 
	{
		Comparator<Cost> comp = new Comparator<Cost>() 
		{
			@Override
			public int compare(Cost c1, Cost c2) {
				return Double.compare(c1.costPerDayMixed, c2.costPerDayMixed);
			}
		};
		return comp;
	}
	
	/**
	 * This is comparator is used to sort the List<Cost> in ascending order according to the local rates only
	 * @return comparator to use in Collections.sort()
	 */
	public static Comparator<Cost> getCompByLocalRates() 
	{
		Comparator<Cost> comp = new Comparator<Cost>() 
		{
			@Override
			public int compare(Cost c1, Cost c2) {
				return Double.compare(c1.costPerDayLocal, c2.costPerDayLocal);
			}
		};
		return comp;
	}
	
	/**
	 * This is comparator is used to sort the List<Cost> in ascending order according to the std rates only
	 * @return comparator to use in Collections.sort()
	 */
	public static Comparator<Cost> getCompBySTDRates() 
	{
		Comparator<Cost> comp = new Comparator<Cost>() 
		{
			@Override
			public int compare(Cost c1, Cost c2) {
				return Double.compare(c1.costPerDaySTD, c2.costPerDaySTD);
			}
		};
		return comp;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Mixed: " + this.costPerDayMixed + ", " + "Local: " + this.costPerDayLocal + ", STD: " + this.costPerDaySTD + ", LocalUsage: " + this.localUsage + ", STDUsage: " + this.stdUsage);            
		return sb.append(this.plan.toString()).toString();
	}
}