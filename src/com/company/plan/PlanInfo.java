package com.company.plan;

/**
 * It only contains a baisc information about the a Plan
 * 
 * It does not depend on any external library. So, we are maintaining a simple structure of a data that we need.
 * 
 * Once this object is created, we don't have to change the properties. So, this is only a read-only class.
 * 
 * 
 * @author sriharish
 *
 */
public class PlanInfo implements Plan
{
	private int cost;
	private int[] callRates = new int[2];
	private String location;
	private String operator;
	private int validity;
	

	public PlanInfo(String operator, String location, int cost, int[] callRates, int validity)
	{
		this.callRates[0] = callRates[0]; // local
		this.callRates[1] = callRates[1]; // std
		this.location = location;
		this.operator = operator;
		this.validity = validity;
		this.cost = cost;
	}
	
	public int getCost()
	{
		return this.cost;
	}
	
	public int[] getCallRates()
	{
		return this.callRates;
	}
	
	public int getValidity()
	{
		return this.validity;
	}
	
	public String getOperator()
	{
		return this.operator;
	}
	
	public String getLocation()
	{
		return this.location;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\nLocation: " + this.location + ", ");
		sb.append("Operator: " + this.operator + ", ");
		sb.append("Cost: Rs. " + this.cost + ", ");
		sb.append("Local: Rs. " + this.callRates[0] + "p\\min, ");
		sb.append("STD: Rs. " + this.callRates[1] + "p\\min, ");
		sb.append("Validity: " + this.validity + "days");
		return sb.toString();
	}
}
