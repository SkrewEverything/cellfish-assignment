package com.company.plan;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.*;
import org.json.simple.parser.*;

/**
 * It gets the data from the disk
 * @author sriharish
 *
 */
public class PlanRequestOffline implements PlanRequest
{
	private String location;
	private String operator;
	
	private PlanRequestOffline()
	{
		// To force the user to inistantiate this class by providing location and operator
	}
	
	public PlanRequestOffline(String location, String operator)
	{
		this.location = location;
		this.operator = operator;
	}
	
	@Override
	public JSONObject getAllPlans()
	{
		Object obj = null;
		try
		{
			obj = new JSONParser().parse(new FileReader("./data/Plan.json"));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		} 
        
        // typecasting obj to JSONObject 
        return (JSONObject) obj; 
	}


	@Override
	public String getCurrentLocation()
	{
		return this.location;
	}

	@Override
	public String getCurrentOperator()
	{
		return this.operator;
	}

	@Override
	public void setCurrentLocation(String location)
	{
		this.location = location;
	}

	@Override
	public void setCurrentOperator(String operator)
	{
		this.operator = operator;
	}

}
