package com.company.plan;
/**
 * It stores the info of the plan
 * 
 * This `interface` should be implemented for storing the plan info 
 * @author sriharish
 *
 */
public interface Plan
{
	/**
	 * Returns the price or cost the plan in Rs.
	 * @return Price or cost the plan
	 */
	int getCost();
	
	/**
	 * Returns the cost the call in paise/min
	 * [0] -> Gives the local call rate
	 * [1] -> Gives the STD call rate
	 * @return Call rates of local and std
	 */
	int[] getCallRates();
	
	/**
	 * Returns the validity of the plan in days
	 * @return Validity of the plan
	 */
	int getValidity();
	
	/**
	 * Returns the operator of the plan
	 * @return operator of the plan
	 */
	String getOperator();
	
	/**
	 * Returns the location of the plan
	 * @return Location of the plan
	 */
	String getLocation();
}
