package com.company.plan;

/**
 * This is the basic interface for getting plan data from the source.
 * 
 * The implementation will be different obviously.
 * Like getting data from database using JDBC
 * Getting data from HTTP server
 * Getting data from disk(like I implemented for simplicity)
 * @author sriharish
 *
 */
public interface PlanRequest
{
	/**
	 * Returns all the plans based on the location and operator
	 * @return All the plans based on the location and operator
	 */
	Object getAllPlans();
	
	/**
	 * Returns the current location of the plans
	 * @return Current location for the plans
	 */
	String getCurrentLocation();
	
	/**
	 * Returns the current operator of the plans
	 * @return Current location for the plans
	 */
	String getCurrentOperator();
	
	/**
	 * Updates the current location for the plans
	 * @param location New location to update
	 */
	void setCurrentLocation(String location);
	
	/**
	 * Updates the current operator for the plans
	 * @param operator New operator to update
	 */
	void setCurrentOperator(String operator);
}
