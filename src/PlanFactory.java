import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.company.plan.Plan;
import com.company.plan.PlanInfo;
import com.company.plan.PlanRequestOffline;


/**
 * PlanFactory deals with multiple sources(for retrieving data) and different types of data formats.
 * 
 * It parses the data from source and returns the `Plan` object
 * @author sriharish
 *
 */
public class PlanFactory
{
	/**
	 * Retrieves data from disk and parses JSON and returns `List<Plan>`
	 * @param location Location/Circle of the phone number
	 * @param operator Operator/Company of the phone number
	 * @return List of all the available `Plan`s
	 */
	public static List<Plan> getPlansFromJSON(String location, String operator)
	{
		JSONObject json = new PlanRequestOffline(location, operator).getAllPlans();
		
		List<Plan> plans = new ArrayList<>();
        JSONArray a = (JSONArray)json.get("plans");
        for(int i = 0; i<a.size(); i++)
        {
        	JSONObject temp = (JSONObject)a.get(i);
        	
        	int[] callRates = new int[2];
        	callRates[0] = Integer.parseInt((String) temp.get("local rate"));
        	callRates[1] = Integer.parseInt((String) temp.get("std rate"));
        	int cost = Integer.parseInt((String) temp.get("cost"));
        	int validity = Integer.parseInt((String) temp.get("validity"));
        	
        	plans.add(new PlanInfo(operator, location, cost, callRates, validity));
        }
        
		return plans;
	}

}
